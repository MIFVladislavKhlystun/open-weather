import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CityInterface} from '../shared/interfaces/city.interface';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.css']
})
export class SearchFieldComponent implements OnInit {

  searchCity = new FormControl();

  @Input() cities: CityInterface[];
  @Input('queryCity') set queryCity(city: string) {
    if (city) {
      this.searchCity.setValue(city);
    }
  }
  @Output() city = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  getData(cityName: string) {
    this.city.emit(cityName);
  }

}

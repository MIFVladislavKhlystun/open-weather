import { Component, OnInit } from '@angular/core';
import {CityInterface} from '../shared/interfaces/city.interface';
import {CitiesDataService} from '../shared/services/cities-data.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  cities: CityInterface[] = [];
  isLoading = true;
  currentCity: string;

  constructor(private citiesService: CitiesDataService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.citiesService.getCities().subscribe(
      cities => {
        this.cities = cities;
        this.isLoading = false;
      }
    );
    this.route.queryParams.subscribe(
      data => {
        if (data.query) {
          this.currentCity = data.query;
        }
      }
    );
  }

  onChooseCity(city: string) {
    this.router.navigate([''], {queryParams: {query: city}});
    this.currentCity = city;
  }
}

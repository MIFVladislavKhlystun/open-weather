export interface ForecastInterface {
  clouds: number;
  humidity: number;
  pressure: number;
  temp: number;
  icon: string;
  weather: string;
  wind: number;
  date: string;
  description: string;
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { CityInterface } from '../interfaces/city.interface';

@Injectable({
  providedIn: 'root'
})
export class CitiesDataService {

  constructor(private http: HttpClient) {
  }

  getCities() {
    return this.http.get<CityInterface[]>('../../assets/UA.min.json')
      .pipe(
        map(
          cities => cities.sort((currentEl, nextEl) => currentEl.name.localeCompare(nextEl.name)
          )
        )
      );
  }
}

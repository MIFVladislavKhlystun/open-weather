import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pressure'
})
export class PressurePipe implements PipeTransform {

  transform(value: number): number {
    const mercuryColumn = 0.75006375541921;
    return value * mercuryColumn;
  }

}

import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(private elementRef: ElementRef,
              private renderer: Renderer2) {
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlightItem('#eee');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlightItem('white');
  }

  private highlightItem(color: string) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', color);
  }
}

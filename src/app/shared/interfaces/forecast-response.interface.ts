import {ForecastResponseItemInterface} from './forecast-response-item.interface';

export interface ForecastResponseInterface {
  list: ForecastResponseItemInterface[];
}

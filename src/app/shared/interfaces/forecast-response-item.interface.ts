export interface ForecastResponseItemInterface {
  clouds: {
    all: number;
  };
  dt_txt: string;
  main: {
    humidity: number;
    pressure: number;
    temp: number;
  };
  weather: [{
    main: string;
    icon: string;
    description: string;
  }];
  wind: {
    speed: number;
  };
}

export interface CityInterface {
  id: number;
  name: string;
  country: string;
  coord: {
    lon: number;
    lat: number;
  };
}

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SearchFieldComponent} from './search-field/search-field.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {SearchPipe} from './shared/pipes/search.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CloseDirective} from './shared/directives/close.directive';
import {HighlightDirective} from './shared/directives/highlight.directive';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {LoaderComponent} from './loader/loader.component';
import {CurrentWeatherComponent} from './current-weather/current-weather.component';
import {AddictiveInfoPipe} from './shared/pipes/addictive-info.pipe';
import {PressurePipe} from './shared/pipes/pressure.pipe';
import {ForecastComponent} from './forecast/forecast.component';
import {ForecastItemComponent} from './forecast/forecast-item/forecast-item.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UpgradeInsecureRequestsInterceptor} from './shared/upgrade-insecure-requests.interceptor';
import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path: '', component: HomeComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SearchFieldComponent,
    SearchPipe,
    CloseDirective,
    HighlightDirective,
    LoaderComponent,
    CurrentWeatherComponent,
    AddictiveInfoPipe,
    PressurePipe,
    ForecastComponent,
    ForecastItemComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ScrollingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UpgradeInsecureRequestsInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

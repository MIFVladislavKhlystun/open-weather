import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

export class UpgradeInsecureRequestsInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const updatedRequest = req.clone({
      url: req.url.replace('http://', 'https://')
    });
    return next.handle(updatedRequest);
  }
}

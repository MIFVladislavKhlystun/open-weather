import { Directive, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appClose]'
})
export class CloseDirective {
  isClosed = true;

  constructor(private elementRef: ElementRef) {
  }

  @HostBinding('class.closed') get closed() {
    return this.isClosed;
  }

  @HostListener('document:click', ['$event'])
  onClick(event: MouseEvent) {
    const targetElement = event.target as HTMLElement;
    if (targetElement && !this.elementRef.nativeElement.contains(targetElement) || this.elementRef.nativeElement.contains(targetElement)) {
      this.isClosed = true;
    }
    if (targetElement.classList.contains('form-control')) {
      this.isClosed = false;
    }
  }
}

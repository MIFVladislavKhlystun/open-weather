import {Component, Input, OnInit} from '@angular/core';
import {ForecastDataService} from '../shared/services/forecast-data.service';
import {CurrentWeatherInterface} from '../shared/interfaces/current-weather.interface';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeatherComponent implements OnInit {

  constructor(private forecastService: ForecastDataService) {
  }

  currentWeather: CurrentWeatherInterface;
  isLoading = false;
  error: string = null;

  ngOnInit() {
  }

  @Input('currentCity') set currentCity(value: string) {
    if (value) {
      this.getCurrentWeather(value);
    }
  }

  getCurrentWeather(city: string) {
    this.isLoading = true;
    this.resetValues();
    this.forecastService.getCurrentWeather(city)
      .subscribe(
        data => {
          this.currentWeather = data;
        },
        error => {
          this.isLoading = false;
          this.error = error;
        },
        () => this.isLoading = false
      );
  }

  resetValues() {
    this.currentWeather = null;
    this.error = null;
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {ForecastDataService} from '../shared/services/forecast-data.service';
import {ForecastInterface} from '../shared/interfaces/forecast.interface';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {

  forecast: ForecastInterface[][] = null;
  isLoading = false;
  error: string;

  constructor(private forecastService: ForecastDataService) {
  }

  @Input('currentCity') set currentCity(value: string) {
    if (value) {
      this.getForecast(value);
    }
  }

  ngOnInit(): void {
  }

  getForecast(city: string) {
    this.isLoading = true;
    this.resetValues();
    this.forecastService.getForecast(city)
      .subscribe(
        data => {
          this.forecast = data;
        },
        error => {
          this.isLoading = false;
          this.error = error;
        },
        () => this.isLoading = false
      );
  }

  resetValues() {
    this.forecast = null;
    this.error = null;
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {ForecastInterface} from '../../shared/interfaces/forecast.interface';

@Component({
  selector: 'app-forecast-item',
  templateUrl: './forecast-item.component.html',
  styleUrls: ['./forecast-item.component.css']
})
export class ForecastItemComponent implements OnInit {
  @Input() forecastItem: ForecastInterface[];
  date: string;
  constructor() {
  }

  ngOnInit() {
    this.forecastItem.map( item => this.date = item.date);
  }

}

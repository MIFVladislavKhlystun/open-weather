import { Pipe, PipeTransform } from '@angular/core';
import {CityInterface} from '../interfaces/city.interface';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(townList: CityInterface[], searchString: string): CityInterface | CityInterface[] {
    if (townList.length === 0 || !searchString) {
      return townList;
    }
    return townList.filter(
      town => town.name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1
    );
  }

}

import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {catchError, map, retry} from 'rxjs/operators';
import {CurrentWeatherResponseInterface} from '../interfaces/current-weather-response.interface';
import {ForecastResponseInterface} from '../interfaces/forecast-response.interface';
import {ForecastInterface} from '../interfaces/forecast.interface';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ForecastDataService {

  private readonly appId = 'df18a6996e3712a30767c950dae692d0';

  constructor(private http: HttpClient) {
  }

  getCurrentWeather(town: string) {
    const params = new HttpParams()
      .set('q', town)
      .set('units', 'metric')
      .set('appid', this.appId);
    return this.http.get<CurrentWeatherResponseInterface>('http://api.openweathermap.org/data/2.5/weather', {params})
      .pipe(
        map(
          info => {
            return {
              clouds: info.clouds.all / 100,
              humidity: info.main.humidity / 100,
              pressure: info.main.pressure,
              temp: info.main.temp,
              sunrise: info.sys.sunrise * 1000,
              sunset: info.sys.sunset * 1000,
              weather: info.weather[0].main,
              icon: `https://openweathermap.org/img/wn/${info.weather[0].icon}@2x.png`,
              wind: info.wind.speed,
              name: info.name,
              description: info.weather[0].description
            };
          }
        ),
        retry(3),
        catchError((err: HttpErrorResponse) => throwError(
          `Server responded with error ${err.error.cod}: ${err.error.message}`)),
      );
  }

  getForecast(town: string) {
    const forecastList: ForecastInterface[] = [];
    const params = new HttpParams()
      .set('q', town)
      .set('units', 'metric')
      .set('appid', this.appId);

    return this.http.get<ForecastResponseInterface>('http://api.openweathermap.org/data/2.5/forecast', {params})
      .pipe(
        map(item => {
          item.list.map(forecastItem => {
            forecastList.push({
              clouds: forecastItem.clouds.all / 100,
              humidity: forecastItem.main.humidity / 100,
              pressure: forecastItem.main.pressure,
              temp: forecastItem.main.temp,
              weather: forecastItem.weather[0].main,
              icon: `https://openweathermap.org/img/wn/${forecastItem.weather[0].icon}.png`,
              wind: forecastItem.wind.speed,
              date: forecastItem.dt_txt,
              description: forecastItem.weather[0].description,
            });
          });
          return forecastList;
        }),
        map(list => {
          let newDate = true;
          const resultArr = [];
          list.forEach((item, ind) => {
              ind++;
              if (ind > list.length - 1) {
                return;
              }
              if (item.date.split(' ')[0] === list[ind].date.split(' ')[0]) {
                if (newDate) {
                  resultArr.push([item, list[ind]]);
                  newDate = false;
                } else {
                  resultArr[resultArr.length - 1].push(list[ind]);
                }
              } else {
                newDate = true;
              }
            }
          );
          return resultArr;
        }),
        retry(3),
        catchError((err: HttpErrorResponse) => throwError(
          `Server responded with error ${err.error.cod}: ${err.error.message}`)),
      );
  }
}


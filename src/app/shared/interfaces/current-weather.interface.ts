export interface CurrentWeatherInterface {
  clouds: number;
  humidity: number;
  pressure: number;
  temp: number;
  sunrise: number;
  sunset: number;
  icon: string;
  weather: string;
  wind: number;
  name: string;
  description: string;
}

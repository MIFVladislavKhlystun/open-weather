export interface CurrentWeatherResponseInterface {
  clouds: {
    all: number
  };
  main: {
    humidity: number;
    pressure: number;
    temp: number;
  };
  sys: {
    sunrise: number;
    sunset: number;
  };
  weather: [{
    icon: string;
    main: string;
    description: string;
  }];
  wind: {
    speed: number;
  };
  name: string;
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addictiveInfo'
})
export class AddictiveInfoPipe implements PipeTransform {

  transform(value: string | number, addictive: string): string | number {
    if (!addictive.length) {
      return value;
    }
    return `${value} ${addictive}`;
  }

}
